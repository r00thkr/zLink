#!/bin/bash
NC=`tput sgr0` 
F_RED=`tput setaf 1` 
B_RED=`tput setab 1` 
F_BLACK=`tput setaf 0` 
B_BLACK=`tput setab 0` 
F_WHITE=`tput setaf 7` 
B_WHITE=`tput setab 7` 
F_GREEN=`tput setaf 2` 
B_GREEN=`tput setab 2` 
F_YELLOW=`tput setaf 3` 
B_YELLOW=`tput setab 3`
ZlinkLoc="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
ZlinkPath=$(<"${ZlinkLoc}/.__zlink_loc")
ZlinkConfiguration=".__zlink_configuration.sh"
PropertiesFileName=("properties")
PropertiesExtTxt=(".txt")
ExitFile=".__zlink_exit"
ValidFileName=""
ValidFile=0
for properties_file in "${PropertiesFileName[@]}"; do
    for properties_ext in "${PropertiesExtTxt[@]}"; do
        if [ -e "${ZlinkPath}/properties/${properties_file}${properties_ext}" ]; then
            ValidFileName="${ZlinkPath}/properties/${properties_file}${properties_ext}"
            (( ValidFile++ ))
        fi
    done
done
clear
if [[ ${ValidFile} -gt 0 ]]; then
    echo "false" > "${ZlinkLoc}/${ExitFile}"
    printf "[  ${F_GREEN}OK${NC}  ]: Properties file found.\n"
    printf "          > ${F_YELLOW}Loading properties...${NC}\n"
    "${ZlinkLoc}/"./"${ZlinkConfiguration}" "${ValidFileName}"
else
    printf "[${F_RED}FAILED${NC}]: No properties file found.\n\n"
    while true; do
        printf "Generate a properties file?\n"
        read -p "[y/n]: " answer
        case ${answer} in
            [Yy]* ) 
                "${ZlinkLoc}/"./"${ZlinkConfiguration}" "generate"
                break;;
            [Nn]* ) 
                echo "true" > "${ZlinkLoc}/${ExitFile}"
                exit;;
            * ) 
                printf "${F_YELLOW}Please answer yes or no.${NC}\n\n";;
        esac
    done
fi

#!/bin/bash
NC=`tput sgr0`
F_RED=`tput setaf 1`
B_RED=`tput setab 1`
F_BLACK=`tput setaf 0`
B_BLACK=`tput setab 0`
F_WHITE=`tput setaf 7`
B_WHITE=`tput setab 7`
F_GREEN=`tput setaf 2`
B_GREEN=`tput setab 2`
F_YELLOW=`tput setaf 3`
B_YELLOW=`tput setab 3`
ZlinkLoc="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
ZlinkPath=$(<"${ZlinkLoc}/.__zlink_loc")
ExitFile=".__zlink_exit"
TargetProperties=""
pause_bash(){
   read -p "$*"
}
pause() {
	pause_bash 'Press [Enter] key to continue...'
}
PropertiesGeneratorHeader() {
    clear
    printf "${F_GREEN}PROPERTIES GENERATOR${NC}\n\n"
}
PathQ() {
    ConfigurationFile=".config_builder"
    Answered=true
    while ${Answered}; do
        PropertiesGeneratorHeader
        printf "${1}\n"
        read -p "> " answer
        case ${answer} in
            * )
                while true; do
                    PropertiesGeneratorHeader
                    printf "${F_YELLOW}${answer}\n${NC}\nIs this path correct?\n"
                    read -p "[y/n]: " validate
                    case $validate in
                        [Yy] )
                            echo "TargetPath=\"${answer}\"" > "${ZlinkLoc}/.properties/${ConfigurationFile}"
                            Answered=false
                        break;;
                        [Nn] )
                            Answered=true
                        break;;
                        * )
                            printf "${F_YELLOW}Please answer yes or no.${NC}\n\n"
                        ;;
                    esac
                done
            ;;
        esac
    done
}
AuthQ() {
    ConfigurationFile=".config_builder"
    SingleAuth=true
    PerAuth=true
    while ${SingleAuth} || ${PerAuth}; do
        PropertiesGeneratorHeader
        printf "${1}\n"
        read -p "[s/p]: " answer
        case ${answer} in
            [Ss] )
                echo "AuthType=\"S\"" >> "${ZlinkLoc}/.properties/${ConfigurationFile}"
                SingleAuth=false
            break;;
            [Pp] )
                echo "AuthType=\"P\"" >> "${ZlinkLoc}/.properties/${ConfigurationFile}"
                PerAuth=false
            break;;
            * )
                printf "${F_YELLOW}Please answer \"single\" or \"per\".${NC}\n\n"
            ;;
        esac
    done
}
PassQ() {
    ConfigurationFile=".config_builder"
    Answer=true
    while true; do
        PropertiesGeneratorHeader
        printf "${1}\n"
        read -p "[y/n]: " answer
        case ${answer} in
            [Yy] )
                while ${Answer}; do
                    PropertiesGeneratorHeader
                    read -p "Password: " -s pass
                    case $pass in
                        * )
                            pass_match=""
                            while [ "${pass}" != "${pass_match}" ]; do
                                echo ""
                                read -p "Re-Type Password: " -s pass_match
                            done
                            while true; do
                                printf "\n\nFinished entering a password?\n"
                                read -p "[y/n]: " finished
                                case $finished in
                                    [Yy] )
                                        echo "UserPass=\"${pass}\"" >> "${ZlinkLoc}/.properties/${ConfigurationFile}"
                                        Answer=false
                                    break;;
                                    [Nn] )
                                    break;;
                                    * )
                                        printf "${F_YELLOW}Please answer yes or no.${NC}\n\n"
                                    ;;
                                esac
                            done
                        ;;
                    esac
                done
            break;;
            [Nn] )
                echo "UserPass=\"\"" >> "${ZlinkLoc}/.properties/${ConfigurationFile}"
            break;;
            * )
                printf "${F_YELLOW}Please answer yes or no.${NC}\n\n"
            ;;
        esac
    done
}
BackupQ() {
    while true; do
        PropertiesGeneratorHeader
        printf "${1}"
        read -p "[y/n]: " answer
        case ${answer} in
            [Yy] )
                echo "BackupOriginal=\"Y\"" >> "${ZlinkLoc}/.properties/${ConfigurationFile}"
            break;;
            [Nn] )
                echo "BackupOriginal=\"N\"" >> "${ZlinkLoc}/.properties/${ConfigurationFile}"
            break;;
            * )
                printf "${F_YELLOW}Please answer yes or no.${NC}\n\n"
            ;;
        esac
    done
}
GeneratePropertiesFile() {
    ConfigurationFile=".config_builder"
    PropertiesQuestions=(
        "What is the full path of the scripts you want to secure? "
        "Do you want \"[S]ingle-time\" or \"[P]er-script\" security on the scripts? "
        "Do you want to supply a password to execute these scripts? "
        "Do you want to backup the original scripts? "
    )
    for question in "${PropertiesQuestions[@]}"; do
        if [ "${question}" == "${PropertiesQuestions[0]}" ]; then
            PathQ "${question}"
        elif [ "${question}" == "${PropertiesQuestions[1]}" ]; then
            AuthQ "${question}"
        elif [ "${question}" == "${PropertiesQuestions[2]}" ]; then
            PassQ "${question}"
        elif [ "${question}" == "${PropertiesQuestions[3]}" ]; then
            BackupQ "${question}"
        fi
    done
    if [ -e "${ZlinkLoc}/.properties/${ConfigurationFile}" ]; then
        if [ ! -e "${ZlinkPath}/properties/properties.txt" ]; then
            mv "${ZlinkLoc}/.properties/${ConfigurationFile}" "${ZlinkPath}/properties/properties.txt"
            TargetProperties="${ZlinkPath}/properties/properties.txt"
        else
            if [ ! -e "${ZlinkPath}/properties/generated_properties.txt" ]; then
                mv "${ZlinkLoc}/.properties/${ConfigurationFile}" "${ZlinkPath}/properties/generated_properties.txt"
                TargetProperties="${ZlinkPath}/properties/properties.txt"
            else
                clear
                printf "[${F_RED}FAILED${NC}]: Naming conflict. Cannot generate a properties file.\n          Please remove, rename or move existing file(s) from \"properties\" folder.\n\n"
            fi
        fi
    fi
}
LoadConfiguration() {
    echo "Loading configuration..."
}
if [ "${1}" == "generate" ]; then
    ConfigurationFile=".config_builder"
    if [ -e "${ZlinkLoc}/.properties/${ConfigurationFile}" ]; then
        rm -rf "${ZlinkLoc}/.properties/${ConfigurationFile}"
    else
        cd .> "${ZlinkLoc}/.properties/${ConfigurationFile}"
    fi
    PropertiesGeneratorHeader
    GeneratePropertiesFile
    rm -rf "${ZlinkLoc}/.properties/${ConfigurationFile}"
elif [ ! -z "${1}" ]; then
    clear
    printf "FILE: ${1}\n\n"
    while true; do
        read -p "Properties detected. Use this file? [y/n]: " answer
        case ${answer} in
            [Yy]* )
                LoadConfiguration "${1}"
                break;;
            [Nn]* )
                echo "true" > "${ZlinkLoc}/${ExitFile}"
                while true; do
                    printf "Generate a properties file instead? "
                    read -p "[y/n]: " answer
                    case ${answer} in
                        [Yy] )
                            ConfigurationFile=".config_builder"
                            if [ -e "${ZlinkLoc}/.properties/${ConfigurationFile}" ]; then
                                rm -rf "${ZlinkLoc}/.properties/${ConfigurationFile}"
                            else
                                cd .> "${ZlinkLoc}/.properties/${ConfigurationFile}"
                            fi
                            PropertiesGeneratorHeader
                            GeneratePropertiesFile
                            rm -rf "${ZlinkLoc}/.properties/${ConfigurationFile}"
                        break;;
                        [Nn] )
                        break;;
                        * )
                            printf "${F_YELLOW}Please answer yes or no.${NC}\n\n"
                        ;;
                    esac
                done
                exit;;
            * )
                printf "${F_YELLOW}Please answer yes or no.${NC}\n\n";;
        esac
    done
fi

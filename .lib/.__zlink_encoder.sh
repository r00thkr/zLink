#!/bin/bash
ZlinkLoc="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
ZlinkPath=$(<"${ZlinkLoc}/.zlink_loc")
GeneratedKeyFile=".zlink_key"

# Takes 1 argument.
# This takes a file as input and then generates a hex code representing the file.
# Then it removes the generated hex codes 0 from the key and writes it to a file.
GeneratedFileKey() {
    xxd -p "${1}" | tr -d '\n' | tr -d '0' > "${ZlinkLoc}/${1}-gfk"
}

# Takes 1 argument.
# This takes a single file as an argument. It takes the generated key and
# writes it the first line of a new generated file, then copies the passed in file 
# into the new generated file and when it is finished it renames the generated file
# to the name of the passed in file with an extension "-mk".
GeneratedMarkFile() {
    CloneFile=".zlink_file_clone"
    FileMarker="# $(head -n 1 "${ZlinkLoc}/${GeneratedKeyFile}")"
    if [ -e "${ZlinkLoc}/${CloneFile}" ]; then
        rm -rf "${ZlinkLoc}/${CloneFile}"
    else
        cd .> "${ZlinkLoc}/${CloneFile}"
    fi
    
    echo "${FileMarker}" > "${ZlinkLoc}/${CloneFile}"
    
    while IFS='' read -r line || [[ -n "$line" ]]; do
        echo "${line}" >> "${ZlinkLoc}/${CloneFile}"
    done < "${1}"
    
    mv "${ZlinkLoc}/${CloneFile}" "${ZlinkLoc}/${1}-mk"
    
    chmod a+x "${ZlinkLoc}/${1}-mk"
}

# Takes 0 arguments.
# When called it will generate a key file that can be used to allow 
# or disallow access to calling this system when it is configured.
GeneratedRandomKey() {
    minimum=1000
    maximum=9999
    number=0
    count=0
    keys=()
    generated_key=""
    
    while [[ ${count} -lt 8 ]]; do
        while [[ ${number} -lt ${minimum} ]]; do
            number=${RANDOM}
            let "number %= ${maximum}"
        done
        (( keys[count]=number ))
        (( count++ ))
        number=0
    done
    for key in "${keys[@]}"; do
        generated_key="${generated_key}${key}"
    done
    echo "${generated_key}" > "${ZlinkLoc}/${GeneratedKeyFile}"
}

if [ "${1}" == "--zlink-generate-default-key" ]; then
    GeneratedRandomKey
elif [ "${1}" == "--zlink-generate-mark-file" ]; then
    GeneratedMarkFile "${2}"
elif [ "${1}" == "--zlink-generate-file-key" ]; then
    GeneratedFileKey "${2}"
fi

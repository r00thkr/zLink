#!/bin/bash
NC=`tput sgr0` 
F_RED=`tput setaf 1` 
B_RED=`tput setab 1` 
F_BLACK=`tput setaf 0` 
B_BLACK=`tput setab 0` 
F_WHITE=`tput setaf 7` 
B_WHITE=`tput setab 7` 
F_GREEN=`tput setaf 2` 
B_GREEN=`tput setab 2` 
F_YELLOW=`tput setaf 3` 
B_YELLOW=`tput setab 3`
ZlinkLoc="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
ZlinkPath=$(<"${ZlinkLoc}/.__zlink_loc")
PropertiesFileName=("properties")
PropertiesExtTxt=(".txt")
ValidFileName=""
ValidFile=0
LoadProperties() {
    clear
    printf "${F_YELLOW}Loading properties...${NC}\n"
}
for properties_file in "${PropertiesFileName[@]}"; do
    for properties_ext in "${PropertiesExtTxt[@]}"; do
        if [ -e "${ZlinkPath}/properties/${properties_file}${properties_ext}" ]; then
            ValidFileName="${ZlinkPath}/properties/${properties_file}${properties_ext}"
            (( ValidFile++ ))
        fi
    done
done
clear
if [[ ${ValidFile} -gt 0 ]]; then
    printf "[  ${F_GREEN}OK${NC}  ]: Properties file found.\n"
    sleep 2s
    LoadProperties
else
    echo "${ZLinkPath}"
    sleep 6s
    clear
    printf "[${F_RED}FAILED${NC}]: No properties file found.\n\nExiting...\n"
fi

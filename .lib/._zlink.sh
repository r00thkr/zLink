#!/bin/bash
NC=`tput sgr0` 
F_RED=`tput setaf 1` 
B_RED=`tput setab 1` 
F_BLACK=`tput setaf 0` 
B_BLACK=`tput setab 0` 
F_WHITE=`tput setaf 7` 
B_WHITE=`tput setab 7` 
F_GREEN=`tput setaf 2` 
B_GREEN=`tput setab 2` 
F_YELLOW=`tput setaf 3` 
B_YELLOW=`tput setab 3`
ZlinkLoc="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
ZlinkPath=$(<"${ZlinkLoc}/.__zlink_loc")
MainFile=".__zlink.sh"
EncFile=".__zlink_pre_encryption_loader.sh"
AuthInitFile="${ZlinkLoc}/._auth_init.sh"
ExitFile=".__zlink_exit"
if [ -e "${ZlinkLoc}/${ExitFile}" ]; then
    rm -rf "${ZlinkLoc}/${ExitFile}"
    cd .> "${ZlinkLoc}/${ExitFile}"
    echo "false" > "${ZlinkLoc}/${ExitFile}"
else
    cd .> "${ZlinkLoc}/${ExitFile}"
    echo "false" > "${ZlinkLoc}/${ExitFile}"
fi
LocationCleanUp() {
    if [ -e  "${ZlinkLoc}/.__zlink_loc" ]; then
        rm -rf "${ZlinkLoc}/.__zlink_loc"
    fi
    if [ -e  "${ZlinkLoc}/${ExitFile}" ]; then
        rm -rf "${ZlinkLoc}/${ExitFile}"
    fi
}
Zlink_Init() {
    if [ -e "${ZlinkLoc}/${MainFile}" ]; then
        "${ZlinkLoc}/"./"${MainFile}"
    else
        printf "\n[${F_RED}FAILED${NC}]: zLink could not start -- Could not find a main file.\n"
    fi
}
Enc_Init() {
    ExecutionStatus=$(<"${ZlinkLoc}/${ExitFile}")
    if [ "${ExecutionStatus}" == "false" ]; then
        if [ -e "${ZlinkLoc}/${EncFile}" ]; then
            "${ZlinkLoc}/"./"${EncFile}"
        else
            printf "\n[${F_RED}FAILED${NC}]: zLink could not start -- Could not execute encryption process.\n"
        fi
    fi
}
Zlink_Init
Enc_Init
LocationCleanUp
